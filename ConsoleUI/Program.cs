﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleUI
{
    class Program
    {
        const int ANTAL_TAL = 20;
        static void Main()
        {
            for (int i = 1; i <= ANTAL_TAL; i++)
            {
                Tal tal = new Tal(i);
                Console.WriteLine($"Tallet {i} er et {tal.LigeEllerUlige()} tal"); 
            }

            Console.ReadKey();
        }
    }
}
