﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleUI
{
    public class Tal
    {
        private int _tal { get; set; }
        public Tal(int tal)
        {
            _tal = tal;
        }

        public string LigeEllerUlige()
        {
            if (_tal % 2 == 0)
            {
                return "lige";
            }
            else
            {
                return "ulige";
            }
        }
    }
}
